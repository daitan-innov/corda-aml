package com.template;

import com.google.common.collect.ImmutableList;
import com.template.contracts.KYCContract;
import com.template.flows.*;
import com.template.states.KYCState;
import com.template.states.LoanState;
import net.corda.core.concurrent.CordaFuture;
import net.corda.core.contracts.Amount;
import net.corda.core.contracts.Command;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.finance.Currencies;
import net.corda.finance.contracts.asset.Cash;
import net.corda.testing.node.MockNetwork;
import net.corda.testing.node.MockNetworkParameters;
import net.corda.testing.node.StartedMockNode;
import net.corda.testing.node.TestCordapp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Currency;
import java.util.HashSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class FlowTests {
    private final MockNetwork network = new MockNetwork(new MockNetworkParameters(ImmutableList.of(
        TestCordapp.findCordapp("com.template.contracts"),
        TestCordapp.findCordapp("com.template.flows"),
        TestCordapp.findCordapp("net.corda.finance.contracts"),
        TestCordapp.findCordapp("net.corda.finance.workflows")
    )));
    private final StartedMockNode a = network.createNode();
    private final StartedMockNode b = network.createNode();

    public FlowTests() {
        a.registerInitiatedFlow(KYCIssueFlowResponder.class);
        a.registerInitiatedFlow(LoanSettleFlowResponder.class);
        b.registerInitiatedFlow(KYCIssueFlowResponder.class);
        b.registerInitiatedFlow(KYCBlacklistFlowResponder.class);
        b.registerInitiatedFlow(LoanIssueFlowResponder.class);
    }

    @Before
    public void setup() {
        network.runNetwork();
    }

    @After
    public void tearDown() {
        network.stopNodes();
    }

    @Test
    public void flowReturnsCorrectlyFormedPartiallySignedTransaction() throws Exception{
        Party manager = a.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();
        Party client = b.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();

        KYCState state = new KYCState(manager, client, 100, null);
        KYCIssueFlowInitiator flow = new KYCIssueFlowInitiator(manager, client);

        Future<SignedTransaction> future = a.startFlow(flow);
        network.runNetwork();

        SignedTransaction ptx = future.get();

        assert(ptx.getTx().getInputs().isEmpty());
        assert(ptx.getTx().getOutputs().get(0).getData() instanceof KYCState);

        Command cmd = ptx.getTx().getCommands().get(0);
        assert(cmd.getValue() instanceof KYCContract.Commands.Issue);
        assert(new HashSet<>(cmd.getSigners()).equals(new HashSet<>(
                state.getParticipants().stream().map(el -> el.getOwningKey())
                        .collect(Collectors.toList()))));

        ptx.verifySignaturesExcept(network.getDefaultNotaryNode().getInfo().getLegalIdentitiesAndCerts().get(0).getOwningKey());
    }

    private SignedTransaction issueKYC() throws InterruptedException, ExecutionException {
        Party manager = a.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();
        Party client = b.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();

        KYCState state = new KYCState(manager, client, 100, null);
        KYCIssueFlowInitiator flow = new KYCIssueFlowInitiator(manager, client);

        CordaFuture future = a.startFlow(flow);
        network.runNetwork();

        return (SignedTransaction) future.get();
    }

    @Test
    public void blacklistFlowReturnsCorrectlyFormedPartiallySignedTransaction() throws Exception{
        //Get the parties
        Party manager = a.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();
        Party client = b.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();

        //Issue a KYC
        SignedTransaction ptx = issueKYC();

        //Lets prepare the blacklist flow
        KYCState output = new KYCState(manager, client, 0, "Some evidence");
        KYCBlacklistFlowInitiator flow = new KYCBlacklistFlowInitiator(output.getManager(), output.getClient(), output.getObservations());

        Future<SignedTransaction> future = a.startFlow(flow);
        network.runNetwork();

        SignedTransaction stx = future.get();
        assert(stx.getTx().getInputs().size() == 1);
        assert(stx.getTx().getOutputs().size() == 1);
        assert(stx.getTx().getOutputs().get(0).getData() instanceof KYCState);
        stx.verifyRequiredSignatures();
    }

    private Cash.State issueCash(Integer amount) throws InterruptedException, ExecutionException {
        SelfIssueCashFlow flow = new SelfIssueCashFlow(amount);
        CordaFuture future = a.startFlow(flow);
        network.runNetwork();
        return (Cash.State)future.get();
    }

    @Test
    public void loanInitiationFlowHappyPath() throws Exception{
        //Get the parties
        Party lender = a.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();
        Party borrower = b.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();

        //Self issue some cash (lender)
        issueCash(10);

        //Create KYC entry for borrower
        issueKYC();

        //Then begin testing the loan
        LoanIssueFlowInitiator loanFlow = new LoanIssueFlowInitiator(10, borrower);
        Future<SignedTransaction> loanFuture = a.startFlow(loanFlow);
        network.runNetwork();
        SignedTransaction stx = loanFuture.get();

        //Given that we had exactly same amount of cash available we should have no change.
        //Expect one input (cash) and two outputs(cash with new owner and the loan state)
        assert(stx.getTx().getInputs().size() == 1);
        assert(stx.getTx().getOutputs().size() == 2);
        assert(stx.getTx().outputsOfType(LoanState.class).size() == 1);
        stx.verifyRequiredSignatures();
    }

    @Test
    public void loanInitiationFlowWithBlacklist() throws Exception{
        //Get the parties
        Party lender = a.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();
        Party borrower = b.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();

        //Self issue some cash (lender)
        issueCash(10);

        //Create KYC entry for borrower
        issueKYC();

        //Blacklist borrower
        //Lets prepare the blacklist flow
        KYCState output = new KYCState(lender, borrower, 0, "Some evidence");
        KYCBlacklistFlowInitiator flow = new KYCBlacklistFlowInitiator(output.getManager(), output.getClient(), output.getObservations());
        Future<SignedTransaction> future = a.startFlow(flow);
        network.runNetwork();
        future.get();

        //Then begin testing the loan
        LoanIssueFlowInitiator loanFlow = new LoanIssueFlowInitiator(10, borrower);
        Future<SignedTransaction> loanFuture = a.startFlow(loanFlow);
        network.runNetwork();
        try{
            loanFuture.get();
            assert(false);
        } catch(ExecutionException e){
            if(e.getCause() instanceof IllegalArgumentException &&
                    e.getCause().getMessage().equals("Borrower failed the KYC validation. Remove from blacklist to perform transaction")) {
                assert (true);
            }
            else {
                assert (false);
            }
        }

    }

    @Test
    public void loanEndToEndFlowHappyPath() throws Exception{
        //Get the parties
        Party lender = a.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();
        Party borrower = b.getInfo().getLegalIdentitiesAndCerts().get(0).getParty();

        //Self issue some cash (lender)
        issueCash(10);

        //Create KYC entry for borrower
        issueKYC();

        //Then begin testing the loan
        LoanIssueFlowInitiator loanFlow = new LoanIssueFlowInitiator(10, borrower);
        Future<SignedTransaction> loanFuture = a.startFlow(loanFlow);
        network.runNetwork();
        loanFuture.get();

        //Then try to settle the loan, the borrower should have enough funds
        LoanSettleFlowInitiator settleFlow = new LoanSettleFlowInitiator(lender);
        Future<SignedTransaction> settling = b.startFlow(settleFlow);
        network.runNetwork();
        SignedTransaction stx = settling.get();

        //We should have the loan and cash as inputs and just the cash (with new owner) as output
        assert(stx.getTx().getInputs().size() == 2);
        assert(stx.getTx().getOutputs().size() == 1);
        assert(stx.getTx().outputsOfType(LoanState.class).size() == 0);
        stx.verifyRequiredSignatures();
    }
}
