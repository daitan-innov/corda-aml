package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableSet;
import com.template.contracts.LoanContract;
import com.template.states.LoanState;
import net.corda.core.contracts.Amount;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.*;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import net.corda.finance.Currencies;
import net.corda.finance.workflows.asset.CashUtils;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

import static net.corda.finance.workflows.GetBalances.getCashBalance;

@InitiatingFlow(version = 2)
@StartableByRPC
public class LoanSettleFlowInitiator extends FlowLogic<SignedTransaction> {
    private final ProgressTracker progressTracker = new ProgressTracker();
    private final Party lender;

    public LoanSettleFlowInitiator(Party lender){
        this.lender = lender;
    }

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException{
        /*
        TODO: Create Flow for the Settle Command! We have provided the code to find the LoanState, but the rest should be coded.
         Step 1 - Check balance has enough funds
         Step 2 - Get notary object
         Step 3 - Create the transaction builder
         Step 4 - Generate the cash expenditure from LoanState information
         Step 5 - Create the Settle Command
         Step 6 - Add the state and command to the transaction
         Step 7 - Validate transaction
         Step 8 - Sign the transaction
         Step 9 - Find the other parties
         Step 10 - Initiate flows (sessions) with the other parties
         Step 11 - Collect signatures using the standard flow
         Step 12 - Notarize transaction
         */
        //Get input state reference from vault
        QueryCriteria queryCriteria = new QueryCriteria.VaultQueryCriteria().withParticipants(Arrays.asList(lender));
        Vault.Page results = getServiceHub().getVaultService().queryBy(LoanState.class, queryCriteria);
        StateAndRef inputStateAndRef = (StateAndRef) results.getStates().get(0);
        LoanState input = (LoanState) inputStateAndRef.getState().getData();

    }

}
