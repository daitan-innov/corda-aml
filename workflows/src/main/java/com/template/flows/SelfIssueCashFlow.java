package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import net.corda.core.contracts.Amount;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.identity.Party;
import net.corda.core.utilities.OpaqueBytes;
import net.corda.finance.Currencies;
import net.corda.finance.contracts.asset.Cash;
import net.corda.finance.flows.AbstractCashFlow;
import net.corda.finance.flows.CashIssueFlow;

import java.util.Currency;

@InitiatingFlow
@StartableByRPC
public class SelfIssueCashFlow extends FlowLogic<Cash.State> {

    Amount<Currency> amount;

    public SelfIssueCashFlow(Integer amount){
        this.amount = Currencies.DOLLARS(amount);
    }

    @Suspendable
    @Override
    public Cash.State call() throws FlowException {
        OpaqueBytes issueRef = OpaqueBytes.of("0".getBytes());
        Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);
        AbstractCashFlow.Result cashIssueTransaction = subFlow(new CashIssueFlow(amount, issueRef, notary));
        return (Cash.State) cashIssueTransaction.getStx().getTx().getOutput(0);
    }
}
