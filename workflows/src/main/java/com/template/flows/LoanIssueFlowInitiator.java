package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableSet;
import com.template.contracts.LoanContract;
import com.template.states.KYCState;
import com.template.states.LoanState;
import net.corda.core.contracts.Amount;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.*;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import net.corda.finance.Currencies;
import net.corda.finance.workflows.asset.CashUtils;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

import static net.corda.finance.workflows.GetBalances.getCashBalance;

@InitiatingFlow(version = 2)
@StartableByRPC
public class LoanIssueFlowInitiator extends FlowLogic<SignedTransaction> {
    private final ProgressTracker progressTracker = new ProgressTracker();

    /*
    TODO: Create variables with the specific types for amount and entity
     */
    private final Amount<Currency> amount;
    private final Party borrower;

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    /*
    TODO: Create initiator constructor that accepts basic types.
     The amount to be borrowed should be an Integer.
     The entity should be a Party.
    */
    public LoanIssueFlowInitiator(Integer value, Party borrower){
        this.amount = Currencies.DOLLARS(value.intValue());
        this.borrower = borrower;
    }


    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException{
        /*
        TODO: Check vault for KYCState with the borrower party.
         */
        //0 - Check if the borrower has a KYC record available in our vault, we won't lend him anything if he doesn't
        QueryCriteria queryCriteria = new QueryCriteria.VaultQueryCriteria().withParticipants(Arrays.asList(borrower));
        Vault.Page results = getServiceHub().getVaultService().queryBy(KYCState.class, queryCriteria);
        StateAndRef inputStateAndRef = (StateAndRef) results.getStates().get(0);
        KYCState state = (KYCState) inputStateAndRef.getState().getData();

        /*
        TODO: Check risk score and throw exception if clause is violated
         */
        if(state.getRiskScore() < 80){
            throw new IllegalArgumentException("Borrower failed the KYC validation. Remove from blacklist to perform transaction");
        }
        /*
        TODO: get cash balance and check that it is higher than the amount we want to lend.
         Throw IllegalArgumentException if the funds are insufficient.
         */
        //The flow is initiated by passing the value and the party that should receive the funds
        //1 - First, lets check that we have sufficient funds
        final Amount<Currency> cashBalance = getCashBalance(getServiceHub(), amount.getToken());
        if(cashBalance.getQuantity() < amount.getQuantity()){
            throw new IllegalArgumentException("Lender doesn't have enough fund to provide");
        }

        //2 - Generate the cash spend and add to the builder (generateSpend encapsulates that)
        Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

        /*
        TODO: Create transaction builder using notary reference.
         */
        TransactionBuilder builder = new TransactionBuilder(notary);

        /*
        TODO: Generate a CashSpend from self to the other party using the amount desired. Function from CashUtils
         */
        CashUtils.generateSpend(getServiceHub(), builder, amount, getOurIdentityAndCert(), borrower, ImmutableSet.of());

        /*
        TODO: Create LoanState to set as output. Use the previously created State.
         */
        //3 - Then, we can issue the LoanState and validate it
        LoanState output = new LoanState(getOurIdentity(), borrower, amount);

        /*
        TODO: Create the issue command to hint contract which type of validation needs to be done.
         */
        final Command<LoanContract.Commands.Issue> issueCommand = new Command<>(new LoanContract.Commands.Issue(),
                output.getParticipants().stream().map(AbstractParty::getOwningKey).collect(Collectors.toList()));

        /*
        TODO: add command and state to transaction using the builder.
         */
        builder.addCommand(issueCommand);
        builder.addOutputState(output, LoanContract.ID);
        builder.verify(getServiceHub());

        //3 - Sign the transaction
        SignedTransaction tx = getServiceHub().signInitialTransaction(builder, getOurIdentity().getOwningKey());

        //4 - Call flow to get the other parties' signatures
        List<Party> otherParties = output.getParticipants().stream().map(el -> (Party)el).collect(Collectors.toList());
        otherParties.remove(getOurIdentity());
        List<FlowSession> sessions = otherParties.stream().map(el -> initiateFlow(el)).collect(Collectors.toList());

        /*
        TODO: Call CollectSignatures flow to reach other participants. Use the signed transaction and the sessions. Save the received transaction
         */
        SignedTransaction stx = subFlow(new CollectSignaturesFlow(tx, sessions));

        /*
        TODO: Call FinalityFlow to notarize the signed transaction received from previous flow
         */
        //5 - Notarize transaction
        return subFlow(new FinalityFlow(stx,sessions));
    }

}
