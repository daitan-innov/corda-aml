package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.template.contracts.KYCContract;
import com.template.states.KYCState;
import net.corda.core.contracts.Command;
import net.corda.core.flows.*;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.CordaX500Name;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;

import java.util.List;
import java.util.stream.Collectors;

// ******************
// * KYCIssueFlowInitiator flow *
// ******************
@InitiatingFlow(version = 2)
@StartableByRPC
public class KYCIssueFlowInitiator extends FlowLogic<SignedTransaction> {
    private final ProgressTracker progressTracker = new ProgressTracker();
    private final KYCState state;

    public KYCIssueFlowInitiator(Party manager, Party client) {
        this.state = new KYCState(manager, client,100, null);
    }

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException {
        //Get notary that will validate the transaction
        final Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

        //Create KYCState using the issue command
        final Command<KYCContract.Commands.Issue> issueCommand = new Command<>(new KYCContract.Commands.Issue(),
                state.getParticipants().stream().map(AbstractParty::getOwningKey).collect(Collectors.toList()));

        //Create transaction
        final TransactionBuilder builder = new TransactionBuilder(notary);

        builder.addOutputState(state, KYCContract.ID);
        builder.addCommand(issueCommand);

        //Verify and sign it with our key
        builder.verify(getServiceHub());
        final SignedTransaction tx = getServiceHub().signInitialTransaction(builder);

        //Collect other parties signatures
        List<Party> otherParties = state.getParticipants().stream().map(el -> (Party)el).collect(Collectors.toList());
        otherParties.remove(getOurIdentity());
        List<FlowSession> sessions = otherParties.stream().map(el -> initiateFlow(el)).collect(Collectors.toList());
        SignedTransaction stx = subFlow(new CollectSignaturesFlow(tx, sessions));

        return subFlow(new FinalityFlow(stx,sessions));
    }
}
