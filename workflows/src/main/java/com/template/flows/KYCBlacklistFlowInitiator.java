package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.template.contracts.KYCContract;
import com.template.states.KYCState;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.*;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;

import java.util.List;
import java.util.stream.Collectors;

// ******************
// * KYCIssueFlowInitiator flow *
// ******************
@InitiatingFlow(version = 2)
@StartableByRPC
public class KYCBlacklistFlowInitiator extends FlowLogic<SignedTransaction> {
    private final ProgressTracker progressTracker = new ProgressTracker();
    private final KYCState output;

    public KYCBlacklistFlowInitiator(Party manager, Party client, String observation) {
        this.output = new KYCState(manager, client,0, observation);
    }

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException {
        //Get input state reference from vault
        QueryCriteria queryCriteria = new QueryCriteria.VaultQueryCriteria().withParticipants(output.getParticipants());
        Vault.Page results = getServiceHub().getVaultService().queryBy(KYCState.class, queryCriteria);
        StateAndRef inputStateAndRef = (StateAndRef) results.getStates().get(0);

        //Get notary that will validate the transaction
        final Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

        //Create KYCState using the blacklist command
        final Command<KYCContract.Commands.Blacklist> blacklistCommand = new Command<>(new KYCContract.Commands.Blacklist(),
                inputStateAndRef.getState().getData().getParticipants().stream().map(AbstractParty::getOwningKey).collect(Collectors.toList()));

        //Create transaction
        final TransactionBuilder builder = new TransactionBuilder(notary);

        builder.addInputState(inputStateAndRef);
        builder.addOutputState(output, KYCContract.ID);
        builder.addCommand(blacklistCommand);

        //Verify and sign it with our key
        builder.verify(getServiceHub());
        final SignedTransaction tx = getServiceHub().signInitialTransaction(builder);

        //Collect other parties signatures
        List<Party> otherParties = inputStateAndRef.getState().getData().getParticipants().stream().map(el -> (Party)el).collect(Collectors.toList());
        otherParties.remove(getOurIdentity());
        List<FlowSession> sessions = otherParties.stream().map(el -> initiateFlow(el)).collect(Collectors.toList());
        SignedTransaction stx = subFlow(new CollectSignaturesFlow(tx, sessions));

        return subFlow(new FinalityFlow(stx,sessions));
    }
}
