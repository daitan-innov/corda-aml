package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.template.states.KYCState;
import net.corda.core.contracts.ContractState;
import net.corda.core.crypto.SecureHash;
import net.corda.core.flows.*;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.utilities.ProgressTracker;

import static net.corda.core.contracts.ContractsDSL.requireThat;

// ******************
// * KYCIssueFlowResponder flow *
// ******************
@InitiatedBy(KYCIssueFlowInitiator.class)
public class KYCIssueFlowResponder extends FlowLogic<SignedTransaction> {
    private FlowSession counterpartySession;
    private SecureHash txWeSigned;

    public KYCIssueFlowResponder(FlowSession counterpartySession) {
        this.counterpartySession = counterpartySession;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException {
        class SignTxFlow extends SignTransactionFlow {
            private SignTxFlow(FlowSession flowSession, ProgressTracker progressTracker){
                super(flowSession, progressTracker);
            }

            @Override
            protected void checkTransaction(SignedTransaction tx){
                requireThat(req -> {
                    ContractState output = tx.getTx().getOutputs().get(0).getData();
                    req.using("This must be an KYC transaction", output instanceof KYCState);
                    return null;
                });

                txWeSigned = tx.getId();
            }
        }

        counterpartySession.getCounterpartyFlowInfo().getFlowVersion();

        SignTxFlow signTxFlow = new SignTxFlow(counterpartySession, SignTransactionFlow.Companion.tracker());

        //Run subflow to actually sign the transaction
        subFlow(signTxFlow);

        return subFlow(new ReceiveFinalityFlow(counterpartySession, txWeSigned));
    }
}
