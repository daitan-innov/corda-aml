package com.template.states;

import com.template.contracts.KYCContract;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.ContractState;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;

import java.util.Arrays;
import java.util.List;

@BelongsToContract(KYCContract.class)
public class KYCState implements ContractState {
    private final Party manager;
    private final Party client;
    private final int riskScore;
    private final String observations;

    public KYCState(Party manager, Party client, int riskScore, String observations){
        this.manager = manager;
        this.client = client;
        this.riskScore = riskScore;
        this.observations = observations;
    }

    @Override
    public List<AbstractParty> getParticipants() {
        return Arrays.asList(manager, client);
    }

    public int getRiskScore() {
        return riskScore;
    }

    public Party getClient() {
        return client;
    }

    public Party getManager() {
        return manager;
    }

    public String getObservations(){
        return observations;
    }
}
