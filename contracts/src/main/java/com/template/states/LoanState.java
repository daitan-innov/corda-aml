package com.template.states;

import com.template.contracts.LoanContract;
import net.corda.core.contracts.Amount;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.ContractState;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

// *********
// * State *
// *********
@BelongsToContract(LoanContract.class)
public class LoanState implements ContractState {
    /*
    TODO: Create class variables using the Party type for entities and Amount<Currency> for values
     */

    /*
    TODO: Create constructor that receive entities and value and sets instance variables
     */

    @Override
    public List<AbstractParty> getParticipants() {
        /*
        TODO: return the participant entities as list
         */
        return new ArrayList<AbstractParty>();
    }

    /*
    TODO: Create getters for internal variables
     */
}