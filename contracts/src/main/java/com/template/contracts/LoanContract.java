package com.template.contracts;

import com.template.states.LoanState;
import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.CommandWithParties;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;

import java.util.Arrays;

import static net.corda.core.contracts.ContractsDSL.requireSingleCommand;
import static net.corda.core.contracts.ContractsDSL.requireThat;

// ************
// * Contract *
// ************
public class LoanContract implements Contract {
    // This is used to identify our contract when building a transaction.
    public static final String ID = "com.template.contracts.LoanContract";

    // A transaction is valid if the verify() function of the contract of all the transaction's input and output states
    // does not throw an exception.
    @Override
    public void verify(LedgerTransaction tx) {
        try{
            final CommandWithParties<Commands> cmd = requireSingleCommand(tx.getCommands(), Commands.class);

            /*
            TODO: Create validators according to the cmd value. Consider Issue and Settle commands
             */
        } catch(IllegalStateException e) {
            throw new IllegalArgumentException("No command from internal class.");
        }
    }

    // Used to indicate the transaction's intent.
    public interface Commands extends CommandData {
        /*
        TODO: Create Issue and Settle Commands to hint validations. Implement the Commands interface
         */
    }
}