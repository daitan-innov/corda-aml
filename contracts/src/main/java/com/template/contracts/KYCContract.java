package com.template.contracts;

import com.template.states.KYCState;
import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.CommandWithParties;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;

import java.util.Arrays;

import static net.corda.core.contracts.ContractsDSL.requireSingleCommand;
import static net.corda.core.contracts.ContractsDSL.requireThat;

public class KYCContract implements Contract {

    public static final String ID = "com.template.contracts.KYCContract";

    @Override
    public void verify(LedgerTransaction tx){
        try{
            final CommandWithParties<Commands> cmd = requireSingleCommand(tx.getCommands(), Commands.class);

            if(cmd.getValue() instanceof Commands.Issue){
                KYCState output = tx.outputsOfType(KYCState.class).get(0);
                requireThat(require -> {
                    require.using("There should be no inputs.", tx.getInputs().size() == 0);
                    require.using("There should be only one output", tx.getOutputs().size() == 1);
                    require.using("The manager and the client must sign", cmd.getSigners().containsAll(Arrays.asList(output.getManager().getOwningKey(), output.getClient().getOwningKey())));
                    return null;
                });
            } else if(cmd.getValue() instanceof Commands.Blacklist){
                KYCState input = tx.inputsOfType(KYCState.class).get(0);
                KYCState output = tx.outputsOfType(KYCState.class).get(0);
                requireThat(require -> {
                    require.using("There should be one inputs.", tx.getInputs().size() == 1);
                    require.using("There should be only one output", tx.getOutputs().size() == 1);
                    require.using("The manager must sign", cmd.getSigners().contains(output.getManager().getOwningKey()));
                    require.using("The riskScore should be less than the initial value", input.getRiskScore() > output.getRiskScore());
                    require.using("The observation must not be empty.", output.getObservations() != null);
                    return null;
                });

            } else {
                throw new IllegalArgumentException("Unrecognized command.");
            }
        } catch(IllegalStateException e){
            throw new IllegalArgumentException("Command is not from local type.");
        }
    }

    public interface Commands extends CommandData {
        class Issue implements Commands {}

        class Blacklist implements Commands {}
    }
}
