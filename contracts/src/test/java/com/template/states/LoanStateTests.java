package com.template.states;

import net.corda.core.contracts.Amount;
import net.corda.core.identity.Party;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;

public class LoanStateTests {
    /**
     * Test state fields
     */
    @Test
    public void hasFieldsOfCorrectType() throws NoSuchFieldException {
        Field lenderField = LoanState.class.getDeclaredField("lender");
        Field borrowerField = LoanState.class.getDeclaredField("borrower");
        Field amountField = LoanState.class.getDeclaredField("amount");

        assertTrue(lenderField.getType().isAssignableFrom(Party.class));
        assertTrue(borrowerField.getType().isAssignableFrom(Party.class));
        assertTrue(amountField.getType().isAssignableFrom(Amount.class));
    }
}
