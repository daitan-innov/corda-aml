package com.template.states;

import net.corda.core.identity.Party;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;

public class KYCStateTests {

    @Test
    public void mustHaveRequiredFields() throws NoSuchFieldException{
        Field manager = KYCState.class.getDeclaredField("manager");
        Field client = KYCState.class.getDeclaredField("client");
        Field riskScore = KYCState.class.getDeclaredField("riskScore");
        Field observations = KYCState.class.getDeclaredField("observations");

        assertTrue(manager.getType().isAssignableFrom(Party.class));
        assertTrue(client.getType().isAssignableFrom(Party.class));
        assertTrue(riskScore.getType().isAssignableFrom(int.class));
        assertTrue(observations.getType().isAssignableFrom(String.class));
    }
}
