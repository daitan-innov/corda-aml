package com.template.contracts;

import com.template.states.KYCState;
import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.TypeOnlyCommandData;
import net.corda.core.identity.CordaX500Name;
import net.corda.testing.core.TestIdentity;
import net.corda.testing.node.MockServices;
import org.junit.Test;

import java.util.Arrays;

import static net.corda.testing.node.NodeTestUtils.ledger;

public class KYCContractTests {
    private final MockServices ledgerServices = new MockServices();

    //Create dummy identities
    public static TestIdentity ALICE = new TestIdentity(new CordaX500Name("Alice", "Testland", "US"));
    public static TestIdentity BOB = new TestIdentity(new CordaX500Name("Bob", "TestCity", "US"));
    public static TestIdentity CHARLIE = new TestIdentity(new CordaX500Name("Charlie", "TestVillage", "US"));

    @Test
    public void mustHaveIssueCommand(){
        KYCState state = new KYCState(ALICE.getParty(), BOB.getParty(), 100, null);

        ledger(ledgerServices, l -> {
            l.transaction(tx -> {
                tx.output(KYCContract.ID, state);
                tx.command(Arrays.asList(BOB.getPublicKey()), new KYCContract.Commands.Issue());
                return tx.failsWith("The manager and the client must sign");
            });
            l.transaction(tx -> {
                tx.output(KYCContract.ID, state);
                tx.command(Arrays.asList(ALICE.getPublicKey(), BOB.getPublicKey()), new KYCContract.Commands.Issue());
                return tx.verifies();
            });
            return null;
        });
    }

    @Test
    public void mustHaveBlacklistCommand(){
        KYCState input = new KYCState(ALICE.getParty(), BOB.getParty(), 100, null);
        KYCState output = new KYCState(ALICE.getParty(), BOB.getParty(), 0 , "Observation text");
        KYCState invalidOutput = new KYCState(ALICE.getParty(), BOB.getParty(), 100 , "Observation text");
        KYCState invalidOutput2 = new KYCState(ALICE.getParty(), BOB.getParty(), 0 , null);

        ledger(ledgerServices, l -> {
            l.transaction(tx -> {
                tx.input(KYCContract.ID, input);
                tx.output(KYCContract.ID, invalidOutput);
                tx.command(Arrays.asList(ALICE.getPublicKey()), new KYCContract.Commands.Blacklist());
                return tx.failsWith("The riskScore should be less than the initial value");
            });
            l.transaction(tx -> {
                tx.input(KYCContract.ID, input);
                tx.output(KYCContract.ID, invalidOutput2);
                tx.command(Arrays.asList(ALICE.getPublicKey()), new KYCContract.Commands.Blacklist());
                return tx.failsWith("The observation must not be empty.");
            });
            l.transaction(tx -> {
               tx.input(KYCContract.ID, input);
               tx.output(KYCContract.ID, output);
               tx.command(Arrays.asList(ALICE.getPublicKey()), new KYCContract.Commands.Blacklist());
               return tx.verifies();
            });
            return null;
        });
    }
}
