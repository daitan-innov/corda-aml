//package com.template.contracts;
//
//import com.template.states.LoanState;
//import net.corda.core.contracts.Amount;
//import net.corda.core.contracts.CommandData;
//import net.corda.core.contracts.TypeOnlyCommandData;
//import net.corda.core.identity.CordaX500Name;
//import net.corda.finance.Currencies;
//import net.corda.testing.core.TestIdentity;
//import net.corda.testing.node.MockServices;
//import org.junit.Test;
//
//import java.util.Arrays;
//import java.util.Currency;
//
//import static net.corda.testing.node.NodeTestUtils.ledger;
//
//public class LoanContractTests {
//    private final MockServices ledgerServices = new MockServices();
//
//    //Create dummy identities
//    public static TestIdentity ALICE = new TestIdentity(new CordaX500Name("Alice", "Testland", "US"));
//    public static TestIdentity BOB = new TestIdentity(new CordaX500Name("Bob", "TestCity", "US"));
//
//    public interface Commands extends CommandData {
//        class DummyCommand extends TypeOnlyCommandData implements Commands {}
//    }
//
//    @Test
//    public void mustRejectUnrecognizedCommand() {
//        Amount<Currency> amount = Currencies.DOLLARS(1);
//        LoanState paper = new LoanState(BOB.getParty(), BOB.getParty(), amount);
//        ledger(ledgerServices, l -> {
//            l.transaction( tx -> {
//                tx.output(LoanContract.ID, paper);
//                tx.command(Arrays.asList(BOB.getPublicKey()), new Commands.DummyCommand());
//                //Fails when the command is unknown
//                return tx.fails();
////                return tx.failsWith("No command from internal class.");
//            });
//            return null;
//        });
//    }
//
//    @Test
//    public void mustIncludeIssueCommand() {
//        // Treat ALICE as the  and BOB as the paper issuer.
//        Amount<Currency> amount = Currencies.DOLLARS(1);
//        LoanState paper = new LoanState(ALICE.getParty(), BOB.getParty(), amount);
//
//        ledger(ledgerServices, l -> {
//            l.transaction( tx -> {
//                tx.output(LoanContract.ID, paper);
//                tx.command(Arrays.asList(BOB.getPublicKey()), new LoanContract.Commands.Issue());
//                return tx.fails();
////                return tx.failsWith("Both parties must sign the transaction");
//            });
//            l.transaction( tx -> {
//                tx.output(LoanContract.ID, paper);
//                tx.command(Arrays.asList(ALICE.getPublicKey(), BOB.getPublicKey()), new LoanContract.Commands.Issue());
//                return tx.verifies();
//            });
//            return null;
//        });
//    }
//
//    @Test
//    public void mustIncludeSettleCommand() {
//        Amount<Currency> amount = Currencies.DOLLARS(1);
//        LoanState paper = new LoanState(ALICE.getParty(), BOB.getParty(), amount);
//
//        ledger(ledgerServices, l -> {
//            l.transaction(tx -> {
//                tx.input(LoanContract.ID, paper);
//                tx.command(Arrays.asList(BOB.getPublicKey()), new LoanContract.Commands.Settle());
//                return tx.fails();
////                return tx.failsWith("Both parties must sign the transaction");
//            });
//            l.transaction(tx -> {
//                tx.input(LoanContract.ID, paper);
//                tx.output(LoanContract.ID, paper);
//                tx.command(Arrays.asList(BOB.getPublicKey(), ALICE.getPublicKey()), new LoanContract.Commands.Settle());
//                return tx.fails();
////                return tx.failsWith("No outputs when settling.");
//            });
//            return null;
//        });
//    }
//}